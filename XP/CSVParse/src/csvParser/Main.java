package csvParser;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

	public static ArrayList<Flower> getFlowers(String[] args) throws IOException {

		File file = new File(args[0]);
		
		Scanner scanner = new Scanner(file.toPath());
		scanner.useDelimiter(",");
		
		Flower current = new Flower();
		
		ArrayList<Flower> flowers = new ArrayList<Flower>(); 
		
		while (scanner.hasNextLine()) {
		
			String string = scanner.nextLine();

			String[] strings = string.split(",");
			
			current.sepalLenght = Double.valueOf(strings[0]);
			current.sepalWidth = Double.valueOf(strings[1]);
			current.petalLenght = Double.valueOf(strings[2]);
			current.petalWidth = Double.valueOf(strings[3]);
			current.className = strings[4];
	
			flowers.add(new Flower(current.sepalLenght, current.sepalWidth, current.petalLenght, current.petalWidth, current.className));
	
		}
		
		return flowers;
	}
	
	public static double getAvg(ArrayList<Double> array) {
		
		double sum = 0;
		
		for(int i = 0; i<array.size(); i++) {
			sum += array.get(i);
		}
		
		return sum/array.size();
		
	}
	
	public static double getMed(ArrayList<Double> array) {
		
		Collections.sort(array);
		
		if (array.size()%2 == 0) {
			return (array.get(array.size()/2)+array.get(array.size()/2+1))/2;
		}
		else {
			return array.get(array.size()/2);
		}
	}
	
	public static HashMap<String, ArrayList<Flower>> arrayOfFlowersToMapOfFlowers(ArrayList<Flower> flowers) {
		
		HashMap<String, ArrayList<Flower>> map = new HashMap<String, ArrayList<Flower>>();
		
		for (int i = 0; i<flowers.size(); i++) {
			String prevName = "";
			if (flowers.get(i).className.equals(prevName)) {
				map.get(flowers.get(i).className).add(flowers.get(i));
				prevName = flowers.get(i).className;
			}
			else {
				map.put(flowers.get(i).className, new ArrayList<Flower>());
				map.get(flowers.get(i).className).add(flowers.get(i));
				prevName = flowers.get(i).className;
			}
		}
		
		return map;
	}
	
	public static HashMap<String, Flower> mapOfFlowersToMapOFAvgFlowers(HashMap<String, ArrayList<Flower>> map){
		HashMap<String, Flower> avgMap = new HashMap<String, Flower>();
		map.forEach((K,V) -> avgMap.put(K, flowerArrayToAvgFlower(K, V)));
		
		return avgMap;
	}
	
	public static HashMap<String, Flower> mapOfFlowersToMapOFMedFlowers(HashMap<String, ArrayList<Flower>> map){
		HashMap<String, Flower> medMap = new HashMap<String, Flower>();
		map.forEach((K,V) -> medMap.put(K, flowerArrayToMedFlower(K, V)));
		
		return medMap;
	}
	
	public static Flower flowerArrayToAvgFlower(String className, ArrayList<Flower> flowers){
		ArrayList<Double> SL = new ArrayList<Double>();
		ArrayList<Double> SW = new ArrayList<Double>();
		ArrayList<Double> PL = new ArrayList<Double>();
		ArrayList<Double> PW = new ArrayList<Double>();
		
		for(int i = 0; i<flowers.size(); i++) {
			SL.add(flowers.get(i).sepalLenght);
		}
		
		for(int i = 0; i<flowers.size(); i++) {
			SW.add(flowers.get(i).sepalWidth);
		}
		
		for(int i = 0; i<flowers.size(); i++) {
			PL.add(flowers.get(i).petalLenght);
		}
		
		for(int i = 0; i<flowers.size(); i++) {
			PW.add(flowers.get(i).petalWidth);
		}
				
		return new Flower(getAvg(SL), getAvg(SW), getAvg(PL), getAvg(PW), className);
		
	} 

	public static Flower flowerArrayToMedFlower(String className, ArrayList<Flower> flowers){
		ArrayList<Double> SL = new ArrayList<Double>();
		ArrayList<Double> SW = new ArrayList<Double>();
		ArrayList<Double> PL = new ArrayList<Double>();
		ArrayList<Double> PW = new ArrayList<Double>();
		
		for(int i = 0; i<flowers.size(); i++) {
			SL.add(flowers.get(i).sepalLenght);
		}
		
		for(int i = 0; i<flowers.size(); i++) {
			SW.add(flowers.get(i).sepalWidth);
		}
		
		for(int i = 0; i<flowers.size(); i++) {
			PL.add(flowers.get(i).petalLenght);
		}
		
		for(int i = 0; i<flowers.size(); i++) {
			PW.add(flowers.get(i).petalWidth);
		}
				
		return new Flower(getMed(SL), getMed(SW), getMed(PL), getMed(PW), className);
		
	} 

	
	public static void customPrintMap(String type, HashMap<String, Flower> map, PrintWriter writer) {
		map.forEach((K,V) -> customPrintFlower(type, K, V, writer));
	}
	
	public static void customPrintFlower(String type, String className, Flower avgFlower, PrintWriter writer) {
		writer.println(className + " sepal lenght " + type + ": " + avgFlower.sepalLenght);
		writer.println(className + " sepal width " + type + ": " + avgFlower.sepalWidth);
		writer.println(className + " petal lenght " + type + ": " + avgFlower.petalLenght);
		writer.println(className + " petal width " + type + ": " + avgFlower.petalWidth);
		writer.println();
	}
	
	public static void main(String[] args) throws IOException {
		
		
		ArrayList<Flower> flowers = getFlowers(args);
		
		HashMap<String, ArrayList<Flower>> map = arrayOfFlowersToMapOfFlowers(flowers); 

		HashMap<String, Flower> avgMap = mapOfFlowersToMapOFAvgFlowers(map);
		HashMap<String, Flower> medMap = mapOfFlowersToMapOFMedFlowers(map);
		
		PrintWriter writer;
		
		if (args.length == 1) {
			writer = new PrintWriter(System.out);
		}
		
		else {
			File file = new File(args[1]);
			writer = new PrintWriter(new PrintStream(file));
		}
		
		customPrintMap(new String("avarage"), avgMap, writer);
		customPrintMap(new String("median"), medMap, writer);

		writer.close();
		
	}
	
}
