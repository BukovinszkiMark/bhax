<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Chomsky!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Full screen</title>
        <para>
Készítsünk egy teljes képernyős Java programot!
        </para>
<programlisting language="java"><![CDATA[
import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Main {
	public static void main(String[] args) {
		
	    GraphicsEnvironment graphics = GraphicsEnvironment.getLocalGraphicsEnvironment();
	    GraphicsDevice device = graphics.getDefaultScreenDevice();
	    
	    JFrame frame = new JFrame("Fullscreen");
	    frame.getContentPane().setBackground(Color.green);
	    
	    JPanel panel = new JPanel();
	    
	    JLabel label = new JLabel("", JLabel.CENTER);
	    label.setText("Hello World! (fullscreen version)");
	    
	    frame.add(panel);
	    frame.add(label);
	    
	    frame.setUndecorated(true);
	    frame.setResizable(false);
	    
	    device.setFullScreenWindow(frame);
	    
	}

}
]]></programlisting>  
        <para>
A program alapját a java által biztosított JFrame, JLabel és JPanel biztosítja, amelyek az elemek elhelyezését kezelik az ablakban. Emellett már csak a használt grafikus eszközt kell lekérnünk és teljesképernyősre állítanunk (szintén a java beépített elemeit használva).
        </para>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/fullscreen-zoom.png" scale="100"/>
                </imageobject>
            </mediaobject>

    </section>        

    <section>
        <title>EPAM: Order of everything</title>
        <para>
Írj olyan metódust, amely tetszőleges Collection esetén vissza adja az elemeket egy List-ben
növekvően rendezve, amennyiben az elemek összehasonlíthatóak velük azonos típusú
objektumokkal. Ha ez a feltétel nem teljesül, az eredményezzen syntax error-t.
        </para>
        <para>
A megoldás 2 részből áll. Az első rész, hogy hogyan nyerjük ki a tartalmat egy tetszőleges Collection-ből. Ez elég egyszerű, mivel az ArrayList konstruktora eleve képes kezelni Collection paramétereket. A második kérdés már csak az, hogy ezeket hogyan fogjuk rendezni? Erre Comparator-t fogunk használni (már ha meg lett írva a tárolt class-hoz). 
        </para>
<programlisting language="java"><![CDATA[
	public static <T extends Comparable<T>> List<T> createOrderedList(Collection<T> collection) {
		
		List<T> list = new ArrayList<T>(collection);
		list.sort((Comparator<? super T>) Comparator.naturalOrder());
		return list;
		
	} 
]]></programlisting>
        <para>
Ha kipróbáljuk konkrét példákon, akkor azt láthatjuk, hogy probléma nélkül tud kezelni tetszőleges Collection-t.  
        </para> 
<programlisting language="java"><![CDATA[
		Collection<Integer> collection = new HashSet<Integer>();
		collection.add(4);
		collection.add(1);
		collection.add(5);
		collection.add(2);
		collection.add(3);
		
		System.out.println(createOrderedList(collection).toString());

		
		Collection<String> collection2 = new PriorityQueue<String>();
		collection2.add("a");
		collection2.add("c");
		collection2.add("abc");
		collection2.add("b");
		collection2.add("cb");
		
		System.out.println(createOrderedList(collection2).toString());
]]></programlisting> 
<screen><![CDATA[
[1, 2, 3, 4, 5]
[a, abc, b, c, cb]
]]></screen>
        <para>
Ha viszont egy olyan class-t használunk, amelyen nem definiáltuk a Comparator-t, akkor az ennek megfelelő syntax error-t kapjuk.
        </para>   
<programlisting language="java"><![CDATA[
		Collection<Animal> collection3 = new ArrayList<Animal>();
		collection3.add(new Animal("RandomName1"));
		collection3.add(new Animal("RandomName2"));
		collection3.add(new Animal("RandomName3"));
		collection3.add(new Animal("RandomName4"));
		collection3.add(new Animal("RandomName5"));
		
		System.out.println(createOrderedList(collection3).toString());
]]></programlisting>  
    </section>        

    <section>
        <title>EPAM: Bináris keresés és Buborék rendezés implementálása</title>
        <para>
Implementálj egy Java osztályt, amely képes egy előre definiált n darab Integer tárolására. Ennek az
osztálynak az alábbi funkcionalitásokkal kell rendelkeznie:
        </para>
	<itemizedlist>
		<listitem>
			<para>
Elem hozzáadása a tárolt elemekhez
			</para>
		</listitem>
		<listitem>
			<para>
Egy tetszőleges Integer értékről tudja eldönteni, hogy már tároljuk-e (ehhez egy bináris keresőt
implementálj)
			</para>
		</listitem>
		<listitem>
			<para>
A tárolt elemeket az osztályunk be tudja rendezni és a rendezett (pl növekvő sorrend) struktúrával
vissza tud térni (ehhez egy buborék rendezőt implementálj)
			</para>
		</listitem>
	</itemizedlist>
<programlisting language="java"><![CDATA[
import java.util.Arrays;

public class IntegerStorage {

	private int[] storage;
	private int index = 0;
	
	public IntegerStorage(int size) {
		this.storage = new int[size];
	}
	
	public IntegerStorage(int[] storage) {
		this.storage = storage;
		this.index = storage.length;
	}
	
	public void add(Integer value) {
		storage[index++] = value;
	}
	
	public boolean contains(Integer value) {
		
		int max = storage.length;
		int min = 0;
		
		while (min <= max) {
			
			int mid = (max+min)/2;
			
			if (mid == value) {
				return true;
			} 
			
			else if (mid > value) {
				max = mid - 1;
			}
			
			else if (mid < value) {
				min = mid + 1;
			}
		}
		return false;
	}
	
	public void sort() {
		for(int i = 0;i<storage.length;i++) {
			for(int j = 1;j<storage.length-i;j++) {
				if (storage[j-1]>storage[j]) {
					int temp = storage[j-1];
					storage[j-1]=storage[j];
					storage[j] = temp;
				}
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(storage);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntegerStorage other = (IntegerStorage) obj;
		if (!Arrays.equals(storage, other.storage))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IntegerStorage [storage=" + Arrays.toString(storage) + "]";
	}
	
}
]]></programlisting>   
        <para>
Végezzünk egy gyors tesztet:
        </para>
<programlisting language="java"><![CDATA[
	public static void main(String[] args) {

		IntegerStorage a = new IntegerStorage(6);
		
		a.add(4);
		a.add(2);
		a.add(6);
		a.add(1);
		a.add(3);
		a.add(5);
		
		a.sort();	
		
		System.out.println(a.toString());

		System.out.println(a.contains(1));
		System.out.println(a.contains(2));
		System.out.println(a.contains(3));
		System.out.println(a.contains(4));
		System.out.println(a.contains(5));
		System.out.println(a.contains(6));
		
		System.out.println(a.contains(9));

]]></programlisting>
<screen><![CDATA[
IntegerStorage [storage=[1, 2, 3, 4, 5, 6]]
true
true
true
true
true
true
false
]]></screen> 
        <para>
Láthatóan működik az add(), sort(), illetve contains() metódusunk ami a feladat tárgyát képezte.
        </para>
    </section>        

    <section>
        <title>EPAM: Saját HashMap implementáció</title>
        <para>
Írj egy saját java.util.Map implementációt, mely nem használja a Java Collection API-t.
Az implementáció meg kell feleljen az összes megadott unit tesztnek, nem kell tudjon kezelni null
értékű kulcsokat és a “keySet”, “values”, “entrySet” metódusok nem kell támogassák az elem
törlést.
        </para>
<programlisting language="java"><![CDATA[
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MyHashMap<K, V> implements Map<K, V> {

	private int size = 0;
	
	private int capacity = 16;
	
	private Node<K, V>[] buckets;
	
	public class Node<K, V> {
		
		K key;
		V value;
		Node next;
		
		public Node(K k, V v) {
			this.key = k;
			this.value = v;
			this.next = null;
		}

	}
	
	public MyHashMap() {
		this.buckets = new Node[capacity];
	}
	
	public MyHashMap(int capacity) {
		this.capacity = capacity;
		this.buckets = new Node[capacity];
	}
	
	public int getBucket(K k) {
		return k.hashCode() % capacity;
	}
	
	@Override
	public void clear() {
		
		Set<K> keys = this.keySet();
		K[] keyArr = (K[]) keys.toArray();
		for (int i = 0; i<=this.size()+1; i++) {
			this.remove(keyArr[i]);
		}
		
	}

	@Override
	public boolean containsKey(Object key) {

		int bucket = getBucket((K)key);
		Node<K, V> current = buckets[bucket];

		while(true) {
			if(current == null) {
				return false;
			}
			else if (current.key.equals(key)) {
				return true;
			}
			else if(!current.key.equals(key)) {
				current = current.next;
			}
		}

	}

	@Override
	public boolean containsValue(Object value) {

		boolean result = false;
		
		for (int i = 0; i<buckets.length; i++) {
			Node current = buckets[i];
				while(result == false) {
					if(current == null) {
						break;
					}
					else if (current.value != null && current.value.equals(value)) {
						result = true;
						break;
					}
					else if(current.value != value) {
						current = current.next;
					}
				}
		}
		
		return result;
		
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		
		Set<Entry<K, V>> set = new HashSet<>();
		
		for (int i = 0; i<buckets.length; i++) {
			Node current = buckets[i];
			while (true) {
				if (current == null) {
					break;
				}
				
				else if (current != null ) {
				set.add(new AbstractMap.SimpleEntry(current.key, current.value));
				current = current.next;
				}
			}
		}
		
		return set;
		
	}

	@Override
	public V get(Object key) {

		int bucket = getBucket((K)key);
		
		Node current = buckets[bucket];
		
		while(true) {
			if(current == null) {
				return null;
			}
			else if (current.key != null && current.key.equals(key)) {
				return (V) current.value;
			}
			else if(current.key != key) {
				current = current.next;
			}
		}
		
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}

	@Override
	public Set<K> keySet() {
		
		Set<K> set = new HashSet<>();
		
		for (int i = 0; i<buckets.length; i++) {
			Node current = buckets[i];
			while (true) {
				if (current == null) {
					break;
				}
				
				else if (current != null ) {
				set.add((K)current.key);
				current = current.next;
				}
			}
		}
		
		return set;

		
	}

	public void resize(K key, V value) {
		System.out.println("resizing");
		MyHashMap<K, V> newMap= new MyHashMap<K, V>(capacity << 1);
		Set<Entry<K, V>> entries = this.entrySet();
		for (Entry<K, V> e : entries) {
			newMap.put(e.getKey(), e.getValue());
		}	
		newMap.put(key, value);
		this.size = newMap.size;
		this.capacity = newMap.capacity;
		this.buckets = newMap.buckets;
	}
	
	@Override
	public V put(K key, V value) {
		
		if (size > capacity*70/100) {
			resize(key, value);
			return null;
		}
		else {
			int bucket = getBucket(key);
			V previousValue;
			
			Node<K, V> current = buckets[bucket];
	
			if (buckets[bucket] == null) {
				previousValue = null; 
				buckets[bucket] = new Node(key, value);
				++size;
			}
			else if (buckets[bucket].key.equals(key)) {
				previousValue = buckets[bucket].value; 
				buckets[bucket] = new Node(key, value);
				++size;
			}
			else {
				while(true) {
					if(current.next == null) {
						previousValue = null; 
						current.next = new Node(key, value);
						++size;
						break;
					}
					else if (current.next.key.equals(key)) {
						previousValue = (V) current.next.value;
						current.next.value = value;
						break;
					}
					else if(!current.next.key.equals(key)) {
						current = current.next;
					}
				}
			}
		
			return previousValue;

		}
		
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
	    K[] keyArr = (K[]) m.keySet().toArray();
	    V[] valueArr = (V[]) m.values().toArray();
	    for (int i = 0; i < m.size(); i++) {
	    	this.put(keyArr[i],valueArr[i]);
	    }
	}

	@Override
	public V remove(Object key) {

		int bucket = getBucket((K)key);
		
		Node current = buckets[bucket];
		Node previous = buckets[bucket];
		V value;
		while(true) {
			if(current == null) {
				value = null; 
				break;
			}
			else if (current.key.equals(key)) {
				if (current == buckets[bucket]) {
					value = (V) current.value; 
					buckets[bucket] = null;
					size--;
					break;
				}
				else if (current != buckets[bucket]) {
					value = (V) current.value;
					previous.next = current.next;
					size--;
					break;
				}
			}
			else if(!current.key.equals(key)) {
				previous = current;
				current = current.next;
			}
		}
		
		return value;
		
	}
	
		
	@Override
	public int size() {
		return size;
	}

	@Override
	public Collection<V> values() {

		Collection<V> collection = new ArrayList();
		
		for (int i = 0; i<buckets.length; i++) {
			Node current = buckets[i];
			while (true) {
				if (current == null) {
					break;
				}
				
				else if (current != null ) {
				collection.add((V)current.value);
				current = current.next;
				}
			}
		}
				
		return collection;

	}
	
}
]]></programlisting>
    </section>        

      
        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
