#!/usr/bin/clisp

(defun factorial_recursive (n)
    (if (= n 0)
    1
    (* n (factorial_recursive(- n 1)))
    )
)

(loop for i from 0 to 20
    do (format t "~D! = ~D~%" i (factorial_recursive i))
)
