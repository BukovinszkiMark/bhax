public class polargen{

    boolean storing = false;
    double number = 2;

    public polargen(){
        storing = false;
    }

	public double create_number(){
        if ( !storing ) {
            double u1,u2,v1,v2,w;
            do {
                u1 = Math.random();
                u2 = Math.random();
                v1 = 2 * u1 - 1;
                v2 = 2 * u2 -1;
                w = v1*v1 + v2*v2;
            } while ( w>1 );
            double r = Math.sqrt ( ( -2*Math.log ( w ) ) /w );
            number = r* v2;
            storing = true;
            return r * v1;
        } else {
            storing= false;
            return number;
        }

	}

	public static void main(String[] args){

		polargen pg = new polargen();
		for (int i=0; i<10; i++){
			System.out.println(pg.create_number());
		}

	}


}


